==============================================
libmjpeg : Android library for MJPEG streaming
==============================================

Description
===========
This is a small library for streaming and displaying MJPEG streams (e.g. the
video from an IP camera). It is not meant to handle connecting to a camera; it
is really just for reading a stream, decoding the stream into JPEG images, and
displaying the images in a custom SurfaceView.

The code here is directly forked from neuralassmebly's `SimpleMjpegView`_
repository, which in turn seems to be heavily based on the source of an answer
from `Stack Overflow on Android and MJPEG`_. The major improvement comes from
replacing Android's ``BitmapFactory.decodeStream()`` with some custom native
code backed by `jpeg8d`_, which is essentialy `libjpeg`_ for Android. See the
file ``LICENSE.md`` for information on using the code in the ``jni`` folder.

All that has been done so far is to turn the neuralassembly's example
application into a library so that it can be used easily in multiple projects.

.. LINKS
.. _`SimpleMjpegView`: https://bitbucket.org/neuralassembly/simplemjpegview
.. _`Stack Overflow on Android and MJPEG`: http://stackoverflow.com/a/3305276/1140122
.. _`jpeg8d`: https://github.com/folecr/jpeg8d
.. _`libjpeg`: http://libjpeg.sourceforge.net/
