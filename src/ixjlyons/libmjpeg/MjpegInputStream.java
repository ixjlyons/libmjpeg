package ixjlyons.libmjpeg;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

public class MjpegInputStream extends DataInputStream {
    private final byte[] SOI_MARKER = { (byte) 0xFF, (byte) 0xD8 };
    private final byte[] EOF_MARKER = { (byte) 0xFF, (byte) 0xD9 };
    private final byte CLE_MARKER = (byte) 0x0d;
    private final String CONTENT_LENGTH = "Content-Length";
    private final static int HEADER_MAX_LENGTH = 100;
    private final static int FRAME_MAX_LENGTH = 40000 + HEADER_MAX_LENGTH;
    
    private int mContentLength = -1;
    byte[] header = new byte[HEADER_MAX_LENGTH];
    byte[] frameData = null;
    int headerLen = -1;
    int headerLenPrev = -1;
    
    int skip = 1;
    int count = 0;
    
    private static final String TAG = "MJPEG";
    private static final boolean DEBUG = true;
    
    static {
        System.loadLibrary("ImageProc");
    }
    public native int pixeltobmp(byte[] jp, int l, Bitmap bmp);
    public native void freeCameraMemory();
    
    public static MjpegInputStream read(String url) {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpResponse res;
        
        HttpGet get = new HttpGet(url);
        get.setHeader("Authorization", getB64Auth("admin", "infiniteint"));
        
        HttpParams params = httpclient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, 2000);
        HttpConnectionParams.setSoTimeout(params, 2000);
        
        try {
            res = httpclient.execute(get);
            
            try {
                return new MjpegInputStream(res.getEntity().getContent()); 
            } catch (IOException e) {
                Log.e("MjpegInputStream read()", "Couldn't get content from camera");
            }            
        } catch (ClientProtocolException e) {
            Log.e("MjpegInputStream read()", "ClientProtocolException");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("MjpegInputStream read()", "Connection to camera timed out");
        }

        return null;
    }
    
    private static String getB64Auth(String login, String pass) {
        String source = login + ":" + pass;
        String ret = "Basic " +
                Base64.encodeToString(source.getBytes(), Base64.URL_SAFE|Base64.NO_WRAP);
        return ret;
    }

    public MjpegInputStream(InputStream in) {
        super(new BufferedInputStream(in, FRAME_MAX_LENGTH));
    }
    
    private int getStartOfSequence(DataInputStream in, byte[] sequence) throws IOException {
        int end = getEndOfSequence(in, sequence);
        return (end < 0) ? (-1) : (end - sequence.length);
    }
    
    private int getEndOfSequence(DataInputStream in, byte[] sequence) throws IOException {
        int seqIndex = 0;
        byte c;
        for (int i = 0; i < FRAME_MAX_LENGTH; i++) {
            c = (byte) in.readUnsignedByte();

            if (c == sequence[seqIndex]) {
                seqIndex++;
                
                if (seqIndex == sequence.length){
                    return i + 1;
                }
            } 
            else {
                seqIndex = 0;
            }
        }
        
        return -1;
    }
    
    private int getEndOfSequenceSimplified(DataInputStream in, byte[] sequence) 
            throws IOException {
        int startPos = mContentLength/2;
        int endPos = 3*mContentLength/2;
         
        skipBytes(headerLen+startPos);

        int seqIndex = 0;
        byte c;
        for(int i = 0; i < endPos-startPos ; i++) {
            c = (byte) in.readUnsignedByte();
            if(c == sequence[seqIndex]) {
                seqIndex++;
                if(seqIndex == sequence.length){
                    return headerLen + startPos + i + 1;
                }
            } 
            else {
                seqIndex = 0;
            }
        }

        return -1;
    }
    
//    private int parseContentLength(byte[] headerBytes) 
//            throws IOException, NumberFormatException {
//        ByteArrayInputStream headerIn = new ByteArrayInputStream(headerBytes);
//        Properties props = new Properties();
//        props.load(headerIn);
//        return Integer.parseInt(props.getProperty(CONTENT_LENGTH));
//    }
    
    private int parseContentLength(byte[] headerBytes) throws NumberFormatException {
        // content-length is 4 or 5 digits (we can tell if the 5th digit is some null byte)
        String contentLengthString = "";
        if (headerBytes[60] == CLE_MARKER) {
            byte[] contentLengthData = { headerBytes[56], headerBytes[57], headerBytes[58], headerBytes[59] };
            try {
                contentLengthString = new String(contentLengthData, "UTF-8");
            } catch (UnsupportedEncodingException e) {}
        }
        else {
            byte[] contentLengthData = { headerBytes[56], headerBytes[57], headerBytes[58], headerBytes[59], headerBytes[60] };
            try {
                contentLengthString = new String(contentLengthData, "UTF-8");
            } catch (UnsupportedEncodingException e) {}
        }

        return Integer.parseInt(contentLengthString);
    }   
    
    public int readMjpegFrame(Bitmap bmp) throws IOException {
        // determine size of header
        mark(FRAME_MAX_LENGTH);
        int headerLen = getStartOfSequence(this, SOI_MARKER);
        reset();

        // read header
        if(header == null || headerLen != headerLenPrev){
            //header = new byte[headerLen];
            if(DEBUG) Log.d(TAG,"header renewed "+headerLenPrev+" -> "+headerLen);
        }
        headerLenPrev = headerLen;
        readFully(header, 0, headerLen);

        // determine size of frame
        int contentLengthNew = -1;
        try {
            contentLengthNew = parseContentLength(header);
        } catch (NumberFormatException nfe) {
            Log.e(TAG, "NumberformatException while parsing content length");
            //contentLengthNew = getEndOfSequenceSimplified(this, EOF_MARKER); 
            contentLengthNew = getEndOfSequence(this, EOF_MARKER); 
        }
        
        mContentLength = contentLengthNew;
        reset();
        
        // read frame data
        if(frameData==null){
            frameData = new byte[FRAME_MAX_LENGTH];
            if(DEBUG) Log.d(TAG,"frameData newed cl="+FRAME_MAX_LENGTH);
        }
        if(mContentLength + HEADER_MAX_LENGTH > FRAME_MAX_LENGTH){
            frameData = new byte[mContentLength + HEADER_MAX_LENGTH];
            if(DEBUG) Log.d(TAG,"frameData renewed cl="+(mContentLength + HEADER_MAX_LENGTH));
        }
        skipBytes(headerLen);
        readFully(frameData, 0, mContentLength);


        return pixeltobmp(frameData, mContentLength, bmp);
    }
    
    public void setSkip(int s){
        skip = s;
    }
}
